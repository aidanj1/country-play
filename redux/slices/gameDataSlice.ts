import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { GameMode } from '../../types'
// import type { RootState } from '../store'

// Define a type for the slice state
interface GameDataState {
  gameMode: GameMode
}

// Define the initial state using that type
const initialState: GameDataState = {
  gameMode: GameMode.SelectGameMode,
}

export const gameDataSlice = createSlice({
  name: 'gameData',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    selectGameMode: (state, action: PayloadAction<GameMode>) => {
      // console.log({action});
      state.gameMode = action.payload
    },
  },
})

export const { selectGameMode } = gameDataSlice.actions

// Other code such as selectors can use the imported `RootState` type
// export const selectCount = (state: RootState) => state.counter.value

export default gameDataSlice.reducer
