import { createSlice, PayloadAction } from '@reduxjs/toolkit'
// import { GameMode } from '../../types'
// import type { RootState } from '../store'

// Define a type for the slice state
interface GameDataState {
  value: number
}

// Define the initial state using that type
const initialState: GameDataState = {
  value: 0
}

export const gameDataSlice = createSlice({
  name: 'gameData',
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    selectGameMode: (state, action: PayloadAction<number>) => {
      // console.log({action});
      console.log(Object.keys(state));
      state.value = action.payload
    },
  },
})

export const { selectGameMode } = gameDataSlice.actions

// Other code such as selectors can use the imported `RootState` type
// export const selectCount = (state: RootState) => state.counter.value

export default gameDataSlice.reducer
