import React from "react";
import { StyleSheet, View } from "react-native";
import { RecoilRoot } from "recoil";
import { Provider } from "react-redux";

import store from "./redux/store";
import Scene13 from "./scenes/Scene13";
import { GameApiRoot } from "./api/GameApi";


/* -------------------------------------------------------------------------- */
/*                                     App                                    */
/* -------------------------------------------------------------------------- */
export default function App() {
  return (
    <RecoilRoot>
      <Provider store={store}>
        <GameApiRoot />
        <View style={styles.container}>
          <Scene13 />
        </View>
      </Provider>
    </RecoilRoot>
  );
}

/* --------------------------------- Styles --------------------------------- */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
  },
});
