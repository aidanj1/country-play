import React, { useEffect, useRef, useState } from "react";
import { Canvas, useFrame, useThree, extend } from "react-three-fiber";
import { Vector3 } from "three";
import { Text } from "troika-three-text";
extend({ Text }); // Extends troika-three-text to be used within react-three-fiber

// function TextBox({ ...props }: React.SVGProps<SVGTextElement>) {
export type TextBoxProps = {
  text: string;
  position?: number[];
  depthTest?: boolean;
  curveRadius?: number;
  fontSize?: number;
};


export const TextBox: React.FC<TextBoxProps> = ({
  text,
  position = [0, 0, 0],
  depthTest = true,
  // curveRadius = 0,
  // fontSize = 12,
  ...props
}) => {
  const refText = useRef();
  const { camera } = useThree();

  const [phi, setPhi] = useState(0);
  const [isGoUp, setIsGoUp] = useState(true);

  // point down = [Pi/2, 0, 0]
  // Other side = [Pi, 0, 0];
  // When pos = [0,0, Z] => [PI * 0, 0, 0];
  // When pos = [0,0,-Z] => [PI, 0, 0];
  // When pos = [0, Y,0] => [-Pi, 0, 0] or [3/2 PI, 0, 0,]
  // When pos = [0,-Y,0] => [-Pi, 0, 0] or [3/2 PI, 0, 0,]
  // When pos = [ X,0,0] => [0, PI / 2, 0]
  // When pos = [-X,0,0] => [0, -PI / 2, 0]

  useEffect(() => {
    // if (phi >= 0 && !isGoUp) {
    //   setIsGoUp(true);
    //   console.log('1');
    // } else if (phi < 1 && isGoUp) {
    //   setIsGoUp(false);
    //   console.log('2');
    // }

    // if (refText?.current?.rotation) {
    //   let rot = refText?.current?.rotation;
    //   const newRot1 = rot[1] + (isGoUp ? 0.001 : -0.001);
    //   refText.current.rotation = [rot[0], newRot1, rot[2]];
    //   // setPhi(phi + (isGoUp ? 0.01 : -0.01));
    // }

    const lookAtVector = new Vector3(position[0], position[1], position[2]);
    lookAtVector.multiplyScalar( 2 );
    // lookAtVector.copy( lookAtVector ).multiplyScalar( 2 );
    
    if (refText?.current?.rotation) {
      refText?.current.lookAt(lookAtVector);
    }
  });

  useEffect(() => {
    // const lookAtVector = new Vector3(0,0,0);

    // if (refText?.current?.rotation) {
    //   refText?.current.lookAt(lookAtVector);
    // }

  },[]);

  // position = [-30, 0, 0];
  const [x, y, z] = position;
  const xAdjust = x === 0 ? 0.000000000001 : x;
  const yAdjust = y === 0 ? 0.000000000001 : y;
  const zAdjust = z === 0 ? 0.000000000001 : z;
  const yzAngleRadians = Math.atan(yAdjust / zAdjust) * -1;
  // const xyAngleRadians = Math.atan(yAdjust / xAdjust) * -1;
  // const yzAngleRadians = Math.atan(yAdjust / zAdjust) * -1;

  // Rotate mesh every frame, this is outside of React without overhead
  useFrame(() => {
    if (refText && refText.current && refText.current.rotation) {

      // refText.current.rotation.x = 0; //-0.707; //refText.current.rotation.x += 0.1;
      // refText.current.rotation.y = refText.current.rotation.y += 0.05;
      // if (refText.current.rotation.y >= Math.PI * 2) {
      //   refText.current.rotation.y = 0;
      // }

      // if (refText.current.rotation.y >= Math.PI * 2) {
      //   refText.current.rotation.setFrom
      // }


      // // US
      // refText.current.rotation.x = yzAngleRadians;
      // refText.current.rotation.y = Math.PI * - 0.27;
      // refText.current.rotation.z = Math.PI * - 0.2



      // When pos = [0,0, Z] => [PI * 0, 0, 0];
      // When pos = [0,0,-Z] => [PI, 0, 0];
      // When pos = [0, Y,0] => [-Pi, 0, 0] or [3/2 PI, 0, 0,]
      // When pos = [0,-Y,0] => [-Pi, 0, 0] or [3/2 PI, 0, 0,]
      // When pos = [ X,0,0] => [0, PI / 2, 0]
      // When pos = [-X,0,0] => [0, -PI / 2, 0]


      // refText.current.rotation.x = 0; //yzAngleRadians;
      // refText.current.rotation.y = Math.PI * 0.5; //Math.PI * 0.07;
      // refText.current.rotation.z = 0; //xyAngleRadians;

      // console.log(refText.current.rotation.y)
      // refText.current.rotation.x = refText.current.rotation.x += 0.1;
      // refText.current.rotation.x = refText.current.rotation.y += 0.01;
    }
  });

  // const hypotenuse = 30;
  // const myZ = 0; //.00000000000000000000001;
  // const myY = 30; //-1 * Math.sqrt(hypotenuse * hypotenuse - myZ * myZ);
  // // const myZ = 27;
  // // const myY = -1 * Math.sqrt(hypotenuse * hypotenuse - myZ * myZ);
  // const myX = 0;
  // const x2y2z2 = myX * myX + myY * myY + myZ * myZ;
  // let sqrtx2y2z2 = Math.sqrt(x2y2z2);
  // sqrtx2y2z2 = sqrtx2y2z2 === 0 ? 0.00000001 : sqrtx2y2z2;
  // const phi = Math.acos(myX / sqrtx2y2z2);

  // arccos(𝑧𝑥2+𝑦2+𝑧2‾‾‾‾‾‾‾‾‾‾‾‾√).

  // console.log({myX, myY, myZ, phi, zyAn

  // const midPos = 30 * 0.70738;
  // position = [0, midPos, midPos];
  // position = [myX, myY, myZ];


  // NOT I have to flip when it gets to a ceratin point
  // State:
  // const rotation = [-0.707, 0, 0];
  // const rotation = [yzAngleRadians, 0, 0];
  const rotation = [yzAngleRadians, phi, 0];
  // const rotation = [yzAngleRadians, phi, 0];
  // const rotation = [yzAngleRadians, 0, 0];

  // const rotation = [Math.PI, 0, Math.PI];
  // const rotation = [0, Math.PI * -0.5, 0];

  // position = [0, 0, -30];
  // const rotation = [Math.PI * 0.5,0,0];
  // const rotation = [0,Math.PI * 2,0];
  // const rotation = [1, 0, 0, 0];
  // const rotation = [0, 0, 0, 0];
  // const [rotation, setRotation] = useState([0, 0, 0, 0]);
  const [opts, setOpts] = useState({
    // font: "Philosopher",
    // fontSize: 12,
    // color: "#99ccff",
    color: "#ffffff",
    outlineColor: "#000000",
    outlineWidth: "3%",
    maxWidth: 300,
    lineHeight: 1,
    letterSpacing: 0,
    textAlign: "center",
    materialType: "MeshPhongMaterial",
  });

  useFrame(() => {
    // refText.lookAt( camera.position );
    // refText?.current?.lookAt([0, 0, 0]);
    // console.log(Object.keys(refText.current));
  });

  const getFormattedText = () => {
    const splitText = text.split(" ");
    return splitText.join("\n");
    // if (text.length > 10) {

    // }
  }

  const formatedText = getFormattedText();

  return (
    // For props see:
    // https://www.npmjs.com/package/troika-three-text?activeTab=readme
    <text
      ref={refText}
      position={position}
      // rotation={rotation}
      {...opts}
      // @ts-ignore (It said text doesn't exist on "text" but it does).
      text={formatedText}
      // curveRadius={curveRadius}
      // fontSize={fontSize}
      // props
      // font={fonts[opts.font]}
      anchorX="center"
      anchorY="middle"
      {...props}
    >
      {opts.materialType === "MeshPhongMaterial" ? (
        <meshPhongMaterial
          attach="material"
          color={opts.color}
          depthTest={depthTest}
        />
      ) : null}
    </text>
  );
};

export default TextBox;
