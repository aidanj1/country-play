import React, { useState, useEffect, useContext, useRef } from "react";
import {
  View,
  StyleSheet,
  StyleProp,
  ViewStyle,
  GestureResponderEvent,
  NativeTouchEvent,
  PanResponder,
} from "react-native";

import { Canvas, CanvasProps, useFrame, useThree } from "react-three-fiber";
import { useRecoilBridgeAcrossReactRoots_UNSTABLE } from "recoil";
import { Camera, Vector3 } from "three";

/** Spherical coordinates for camera */
export type SphericalCoordinates = [r: number, phi: number, theta: number];

/** Context */
const context = React.createContext(null);

const getSphericalCoordinates = ({
  x,
  y,
  z,
}: Vector3): SphericalCoordinates => {
  // ρ2=𝑥2+𝑦2+𝑧2
  const radius = Math.sqrt(x * x + y * y + z * z);
  // tanθ = 𝑦/𝑥
  const phi = x === 0 ? Math.PI / 2 : Math.atan(y / x);
  // φ = arccos(𝑧 / radius).
  const theta = radius === 0 ? 0 : Math.acos(z / radius);

  return [radius, phi, theta];
};

const CameraHandler = ({ setCamera }) => {
  const { camera } = useThree();
  const { currentCameraSphericalCoords } = useContext(context);
  const refCurrentCameraSphericalCoords = useRef(currentCameraSphericalCoords);
  refCurrentCameraSphericalCoords.current = currentCameraSphericalCoords;

  useEffect(() => {
    setCamera(camera);
  }, [camera]);

  useFrame((state) => {
    const [sphericalRadius, sphericalPhi, sphericalTheta] =
      refCurrentCameraSphericalCoords.current || [];

    // console.log({ sphericalRadius, sphericalPhi, sphericalTheta });
    state.camera.position.setFromSphericalCoords(
      sphericalRadius,
      sphericalPhi,
      sphericalTheta
    );
    state.camera.lookAt(0, 0, 0);
  });

  return null;
};

interface CanvasWithOrbitControlsProps extends CanvasProps {
  viewStyle?: StyleProp<ViewStyle>;
  // style?: StyleProp<ViewStyle>;
}

export const CanvasWithOrbitControls: React.FC<CanvasWithOrbitControlsProps> =
  ({ viewStyle, children, ...canvasProps }) => {
    const RecoilBridge = useRecoilBridgeAcrossReactRoots_UNSTABLE();

    const [camera, setCamera] = useState<Camera>(null);
    // const [newPos, setNewPos] = useState(10);

    /** The initial camera spherical coordinates when a move / zoom starts */
    const [initialCameraSphericalCoords, setInitialCameraSphericalCoords] =
      useState<SphericalCoordinates>([0, Math.PI / 2, 0]);

    /** The camera's currently displayed spherical coordinates as touch(es) move / zoom  */
    const [currentCameraSphericalCoords, setCurrentCameraSphericalCoords] =
      useState<SphericalCoordinates>([0, Math.PI / 2, 0]);

    /** A list of touch ids that are used during a move / zoom */
    const [initialTouchesById, setInitialTouchesById] = useState<{
      [key: string]: NativeTouchEvent;
    }>({});

    /**
     * The initial distance of two touches.
     * This is useful to determine amount of zoom necessary.
     */
    const [initialTouchesDistance, setInitialTouchesDistance] =
      useState<number>(0);

    /*********/

    /**
     * Once the camera is initialized, set the spherical coordinates
     * variable from the camera's current position.
     */
    useEffect(() => {
      const cameraPosition = camera?.position;
      if (cameraPosition) {
        const newSphericalCoordinates = getSphericalCoordinates(cameraPosition);
        setCurrentCameraSphericalCoords(newSphericalCoordinates);
      }
    }, [camera]);

    /*********/

    const getDistanceOfTwoTouches = (touches: NativeTouchEvent[]) => {
      if (touches.length !== 2) {
        return 0;
      }

      const { locationX: x0, locationY: y0 } = touches[0];
      const { locationX: x1, locationY: y1 } = touches[1];
      const width = x1 - x0;
      const height = y1 - y0;

      return Math.sqrt(width * width + height * height);
    };

    const getTouchesDistance = (touches: NativeTouchEvent[]) => {
      if (!touches || touches.length <= 1) {
        return 0;
      }

      if (touches.length === 2) {
        return getDistanceOfTwoTouches(touches);
      }
    };

    /*********/

    const setTouchesAsInitialState = (touches: NativeTouchEvent[]) => {
      let newStartTouches = {};

      touches.forEach((touch) => {
        const { identifier } = touch;
        newStartTouches[identifier] = touch;
      });

      setInitialTouchesById(newStartTouches);
      setInitialCameraSphericalCoords(currentCameraSphericalCoords);
      // setInitialCameraSphericalCoords(getSphericalCoordinates(camera?.position));
      setInitialTouchesDistance(getTouchesDistance(touches));
    };

    // Depending on dx, dy (and possibly pinch / zoom later)
    const setNewCameraSphericalCoordsFromChange = ({
      dx = 0,
      dy = 0,
      dZoom = 1,
    }: {
      /** Change in x direction */
      dx: number;
      /** Change in y direction */
      dy: number;
      /** Change in zoom */
      dZoom: number;
    }) => {
      const [initialRadius, initialPhi, initialTheta] =
        initialCameraSphericalCoords;

      /**
       * SPHERICAL_PHI_TINY_ADJUSTMENT is used because when at 0, the rotate doesn't work right
       * But at some value like 0.00...1 seems to work well.
       */
      const SPHERICAL_PHI_TINY_ADJUSTMENT = 0.00001;
      const SPHERICAL_PHI_MAX = Math.PI - SPHERICAL_PHI_TINY_ADJUSTMENT;
      const SPHERICAL_PHI_Min = SPHERICAL_PHI_TINY_ADJUSTMENT;

      /** How far out you can zoom */
      const MAX_CAMERA_ORBIT_RADIUS = 250;
      /** How far in you can zoom */
      const MIN_CAMERA_ORBIT_RADIUS = 40;
      const sphericalRadius = Math.min(
        MAX_CAMERA_ORBIT_RADIUS,
        Math.max(MIN_CAMERA_ORBIT_RADIUS, initialRadius * dZoom)
      );

      const ANGLE_MULTIPLIER = Math.PI * -0.00002 * sphericalRadius; // Arbitrary multiplier value from trial and error

      /** Midpoint is Math.PI / 2 */
      // let sphericalPhi = dy * ANGLE_MULTIPLIER + SPHERICAL_PHI_MIDPOINT + initialPhi; // x-y plane (the up / down) (currently 0 -> 100)
      let sphericalPhi = dy * ANGLE_MULTIPLIER + initialPhi; // x-y plane (the up / down) (currently 0 -> 100)
      sphericalPhi = Math.min(
        SPHERICAL_PHI_MAX,
        Math.max(SPHERICAL_PHI_Min, sphericalPhi)
      );
      const sphericalTheta = dx * ANGLE_MULTIPLIER + initialTheta; // y-z plane (the side to side

      setCurrentCameraSphericalCoords([
        sphericalRadius,
        sphericalPhi,
        sphericalTheta,
      ]);
    };

    /***********/

    /** Handle moving camera based on single finger drag */
    const handleSingleTouchMove = (touch: NativeTouchEvent) => {
      const { identifier, locationX, locationY } = touch;

      const initialTouch = initialTouchesById[identifier];

      const { locationX: initialLocationX, locationY: initialLocationY } =
        initialTouch;

      const dx = locationX - initialLocationX;
      const dy = locationY - initialLocationY;

      setNewCameraSphericalCoordsFromChange({ dx, dy, dZoom: 1 });

      // Now set that the current number of touches is 1;
      // setNumberOfFingersInUse(NumTouches.one);
    };

    const handleMultiTouchMove = (touches: NativeTouchEvent[]) => {
      // setNumberOfFingersInUse(NumTouches.many);
      let dZoom = 1;

      if (touches && touches.length === 2 && initialTouchesDistance > 0) {
        const currentTouchesDistance = getTouchesDistance(touches);

        /** Ensure the current touch distance isn't */
        if (!(currentTouchesDistance > 0)) {
          return;
        }
        dZoom = initialTouchesDistance / currentTouchesDistance;
        setNewCameraSphericalCoordsFromChange({ dx: 0, dy: 0, dZoom });
      }
    };

    /***********/

    // const handleTouchStart = (e: GestureResponderEvent) => {
    const handleTouchStart = (event: GestureResponderEvent) => {
      const { nativeEvent } = event;
      const { touches } = nativeEvent;

      setTouchesAsInitialState(touches);
    };

    /**
     * Whenever a touch is done we will reset where the initial
     * touches started (because the fingers may be in a new location).
     */
    const handleTouchEnd = (event: GestureResponderEvent) => {
      const { nativeEvent } = event;
      const { touches } = nativeEvent;

      setTouchesAsInitialState(touches);

      // let newStartTouches = {};

      // // For each remaining touch, ensure that touch stays as an initialTouchesById
      // // (i.e. the one that ended will be removed from initialTouchesById)
      // touches.forEach(({ identifier }) => {
      //   // if (initialTouchesById[identifier]) {
      //   newStartTouches[identifier] = initialTouchesById[identifier];
      //   // }
      // });
      // setInitialTouchesById(newStartTouches);
      // setInitialCameraSphericalCoords(currentCameraSphericalCoords);
    };

    const handleTouchMove = (event: GestureResponderEvent) => {
      const { nativeEvent } = event;
      const { touches } = nativeEvent;
      const numTouches = touches.length;

      if (numTouches === 1) {
        return handleSingleTouchMove(touches[0]);
      }

      if (numTouches > 1) {
        return handleMultiTouchMove(touches);
      }
    };

    const panResponder = React.useRef(
      PanResponder.create({
        // Ask to be the responder:
        onStartShouldSetPanResponder: (evt, gestureState) => true,
        onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
        onMoveShouldSetPanResponder: (evt, gestureState) => true,
        onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

        onPanResponderGrant: (evt, gestureState) => {
          // The gesture has started. Show visual feedback so the user knows
          // what is happening!
          // gestureState.d{x,y} will be set to zero now
        },
        onPanResponderMove: (evt, gestureState) => {
          // The most recent move distance is gestureState.move{X,Y}
          // The accumulated gesture distance since becoming responder is
          // gestureState.d{x,y}
        },
        onPanResponderTerminationRequest: (evt, gestureState) => true,
        onPanResponderRelease: (evt, gestureState) => {
          // The user has released all touches while this view is the
          // responder. This typically means a gesture has succeeded
        },
        onPanResponderTerminate: (evt, gestureState) => {
          // Another component has become the responder, so this gesture
          // should be cancelled
        },
        onShouldBlockNativeResponder: (evt, gestureState) => {
          // Returns whether this component should block native components from becoming the JS
          // responder. Returns true by default. Is currently only supported on android.
          return true;
        },
      })
    ).current;

    return (
      <View
        style={[styles.container, viewStyle]}
        onTouchStart={handleTouchStart}
        onTouchMove={handleTouchMove}
        onTouchEnd={handleTouchEnd}
      >
        <Canvas {...canvasProps}>
          {/* <context.Provider value={[newPos, setNewPos]}> */}
          <context.Provider value={{ currentCameraSphericalCoords }}>
            <RecoilBridge>
              <CameraHandler setCamera={setCamera} />
              {children}
            </RecoilBridge>
          </context.Provider>
        </Canvas>
      </View>
    );
  };

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default CanvasWithOrbitControls;
