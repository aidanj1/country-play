import React, { useRef, useState } from "react";
// import { StyleSheet, View } from 'react-native';
// import { useFrame, useLoader } from "react-three-fiber";
// import { SphereBufferGeometry } from "three";
import { TextureLoader } from "expo-three";
import { COUNTRY_ID_GLOBE, PLANET_RADIUS_SPHERE } from "../../../constants";
// import { GameApi } from "../../../api/GameApi";
// import { gameModeState } from "../../../recoil/gameData/atom";
// import { GameMode } from "../../../types";
// import { useRecoilValue } from "recoil";

// const earthImgSrc = require("../../../assets/images/earth/earth_natural.jpg");
const earthNaturalImgSrc = require("../../../assets/images/earth/earth.png");
// const earthImgSrc = require("../../../assets/images/earth/earth_night.jpg");
const earthNightImgSrc = require("../../../assets/images/earth/earth_night-min.png");
const earthImgSrc = require("../../../assets/images/earth/3_no_ice_clouds_8k-min.png");
// const earthImgSrc = require("../../../assets/images/earth/earth.png");
// const earthImgSrc = require("../../../assets/images/earth/earth.jpeg");
// const moonImgSrc = require("../../../assets/images/earth/moon.png");

// const earthTexture = new TextureLoader().load(require("./icon.jpg"));
const earthNaturalTexture = new TextureLoader().load(earthNaturalImgSrc);
const earthNightTexture = new TextureLoader().load(earthNightImgSrc);
const earthTexture = new TextureLoader().load(earthImgSrc);
// const moonTexture = new TextureLoader().load(moonImgSrc);

export const Sphere = (props) => {
  const gameMode = "test"; //useRecoilValue(gameModeState);
  // This reference will give us direct access to the mesh
  const mesh = useRef();

  // Set up state for the hovered and active state
  const [active, setActive] = useState(false);

  // const [texture, moon] = useLoader(TextureLoader, [earthImgSrc, moonImgSrc]);

  // const j = (
  //   <mesh>
  //     <sphereGeometry args={[5, 32, 32]} />
  //     <meshStandardMaterial map={earthTexture} roughness={1} fog={false} />
  //   </mesh>
  // );
  const scale = 1;
  // const scale = PLANET_RADIUS_SPHERE;
  // const scale2 = scale * 1.1;

  const handleClick = () => {
    // GameApi.clickedCountry(COUNTRY_ID_GLOBE);
    console.log('clicked planet4');
  };

  const meshTexture = earthTexture;
  // const meshTexture = gameMode === GameMode.SelectContinent ? earthTexture : earthNaturalTexture;
  // const meshTexture = gameMode === GameMode.SelectContinent ? earthTexture : earthNightTexture;

  return (
    <mesh
      {...props}
      ref={mesh}
      scale={[scale, scale, scale]}
      // scale={active ? [scale, scale, scale] : [scale2, scale2, scale2]}
      // rotateOnAxis={}
      // scale={[scale, scale, scale]}
      // onClick={(e) => setActive(!active)}

      /** calls clickedCountry with -1 which is */
      onClick={handleClick}
    >
      {/* <sphereBufferGeometry attach="geometry" args={[1, 1, 1]} /> */}
      <sphereBufferGeometry
        attach="geometry"
        args={[PLANET_RADIUS_SPHERE, 64, 64]}
      />
      {/* <sphereBufferGeometry attach="geometry" args={[1, 64, 64]} /> */}
      {/* <meshStandardMaterial attach="material" color={"blue"} /> */}
      <meshStandardMaterial map={meshTexture} roughness={1} fog={false} />
    </mesh>
  );
};

export default Sphere;
