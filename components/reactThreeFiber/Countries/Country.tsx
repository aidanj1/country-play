import React, { useMemo, memo } from "react";
import * as THREE from "three";
import { useRecoilValue } from "recoil";
// import { useUpdate, useFrame } from "react-three-fiber";

// import { GLOBE_COUNTRY_SCALE } from "../../../constants";
// import brazilCoords from "../../../data/brazilCoords";
import {
  getCountryCoordinatesFromId,
  getCountrySubregionFromId,
  getCountryContinentFromId,
  getCountryNameFromId,
  isValidCountryId,
  getCenterSphereCoordinateForCountryId,
} from "../../../libs/geoJson/geoJSONHelpers";
import CountryBufferGeometry from "../../../libs/threeJs/CountryBufferGeometry";
import {
  // COUNTRIES_DATA_FEATURES,
  // NUM_COUNTRIES,
  // COUNTRIES_SUBREGIONS,
  // COUNTRIES_CONTINENTS,
  COUNTRIES_SUBREGIONS_COLORS,
  COUNTRIES_CONTINENTS_COLORS,
} from "../../../constants";
import TextBox from "../../TextBox";
// import { BufferGeometry } from "three";
import { GameApi } from "../../../api/GameApi";
import { CountryStatus, GameMode } from "../../../types";
import { colorShade } from "../../../libs/utils";
import {
  countryStatusState,
  subregionCountryStatusFamilyState,
} from "../../../recoil/countries/atom";
import { gameModeState } from "../../../recoil/gameData/atom";

/**
 * Sample of drawing edges, from:
 * https://codesandbox.io/s/r3f-basic-demo-forked-hutul?file=/src/App.js:143-976
 */

/* ---------------------------------- Types --------------------------------- */
type CountryModelProps = {
  myCountryBufferGeometry?: THREE.BufferGeometry;
  onClick?: any;
  selected?: boolean;
  countryId: number;
};

/* -------------------------------- Country Model ------------------------------- */
const CountryModel: React.FC<CountryModelProps> = memo(
  ({ myCountryBufferGeometry, onClick, selected, countryId }) => {
    const edges = useMemo(
      () => new THREE.EdgesGeometry(myCountryBufferGeometry, 10000),
      [myCountryBufferGeometry]
    );
    const scale = 30;

    const subregion = getCountrySubregionFromId(countryId);
    const continent = getCountryContinentFromId(countryId);
    const subregionColor = COUNTRIES_SUBREGIONS_COLORS[subregion];
    const continentColor = COUNTRIES_CONTINENTS_COLORS[continent];

    const meshColor = subregionColor;
    const lineColor = colorShade(subregionColor, -160);

    return (
      <group dispose={null} scale={[scale, scale, scale]}>
        <mesh onClick={onClick} geometry={myCountryBufferGeometry}>
          <meshStandardMaterial
            // color={selected ? "#000" : new THREE.Color("#99FF22")}
            color={new THREE.Color(meshColor)}
            // color={selected ? "#000" : new THREE.Color(subregionColor)}
            opacity={selected ? 0.9 : 0.3}
            transparent
          />
        </mesh>
        <lineSegments geometry={edges}>
          {/* <lineBasicMaterial color="black" depthTest={false} /> */}
          <lineBasicMaterial color={lineColor} linewidth={1.5} />
          {/* <lineBasicMaterial color="black" linewidth={1.5} /> */}
        </lineSegments>
      </group>
    );
  }
);

/* ---------------------------------- Types --------------------------------- */
type CountryProps = {
  countryId: number;
};

/* ---------------------------- Country Component --------------------------- */
const Country: React.FC<CountryProps> = memo(({ countryId }) => {
  // console.log(countryId);
  // Data from recoil stores
  const gameMode = useRecoilValue(gameModeState);
  const countryStatus = useRecoilValue(countryStatusState(countryId));
  const subregion = getCountrySubregionFromId(countryId);
  const subregionCountryStatus = useRecoilValue(
    subregionCountryStatusFamilyState(subregion)
  );

  // Extrapolated data from recoil stores
  const selected =
    countryStatus === CountryStatus.selected ||
    subregionCountryStatus === CountryStatus.selected;
  const isShowText = gameMode !== GameMode.SelectSubregion;

  if (isValidCountryId(countryId)) {
    return null;
  }

  const onCountryClick = () => {
    GameApi.clickedCountry(countryId);
  };

  // const scale = GLOBE_COUNTRY_SCALE;
  const coords = getCountryCoordinatesFromId(countryId);

  const myCountryBufferGeometry = new CountryBufferGeometry();
  myCountryBufferGeometry.setCoordinates(coords);

  const name = getCountryNameFromId(countryId);

  const nameSphereCoordVector3 =
    selected && getCenterSphereCoordinateForCountryId({ countryId });

  const {
    x: nameSphereCoordX,
    y: nameSphereCoordY,
    z: nameSphereCoordZ,
  } = nameSphereCoordVector3 || {};

  const nameSphereCoordPosition = nameSphereCoordVector3 && [
    nameSphereCoordX,
    nameSphereCoordY,
    nameSphereCoordZ,
  ];

  return (
    <group>
      {isShowText && selected && nameSphereCoordPosition && (
        <TextBox
          text={name}
          depthTest={false}
          curveRadius={-30}
          // rotation={[0,0,0]}
          // position={[0, 0, 30]}
          position={nameSphereCoordPosition}
          fontSize={1.5}
        />
      )}
      <CountryModel
        selected={selected}
        onClick={onCountryClick}
        countryId={countryId}
        myCountryBufferGeometry={myCountryBufferGeometry}
      />
    </group>
  );
});

export default Country;
