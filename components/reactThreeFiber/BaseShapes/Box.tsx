import React, { useRef, useState } from "react";
// import { StyleSheet, View } from "react-native";
import { useFrame } from "react-three-fiber";
// import OrbitControlsView from "expo-three-orbit-controls";
// import { OrbitControls } from "drei";

function Box(props) {
  // This reference will give us direct access to the mesh
  const mesh = useRef();

  // Set up state for the hovered and active state
  const [hovered, setHover] = useState(false);
  const [active, setActive] = useState(false);

  // Rotate mesh every frame, this is outside of React without overhead
  useFrame(() => {
    if (mesh && mesh.current) {
      // mesh.current.rotation.x = mesh.current.rotation.y += 0.01;
    }
  });

  const handleClick = (e) => {
    setActive(!active);
    props?.onClick?.();
  }

  const size = props?.size ?? 10;

  return (
    <mesh
      {...props}
      ref={mesh}
      scale={active ? [1.5, 1.5, 1.5] : [1, 1, 1]}
      onClick={handleClick}
      onPointerOver={(e) => setHover(true)}
      onPointerOut={(e) => setHover(false)}
    >
      <boxBufferGeometry attach="geometry" args={[size, size, size]} />
      <meshStandardMaterial
        attach="material"
        color={hovered ? "hotpink" : "orange"}
      />
    </mesh>
  );
}

export default Box;


// import React, { useState, useEffect, useRef } from "react";

// type BoxProps = {
//   sizeAll: number;
// };

// export const Box: React.FC<BoxProps> = ({ sizeAll }) => {
//   return (
//     <mesh position={[0, 0, 0]}>
//       <boxGeometry args={[sizeAll, sizeAll, sizeAll]} />
//       <meshStandardMaterial color="blue" />
//     </mesh>
//   );
// };

// export default Box;
