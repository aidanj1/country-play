export type diffPosition = { dx: number; dy: number };

export type sphericalCoordinates = [r: number, phi: number, theta: number];

export enum ConversionShape {
  "sphere" = "sphere",
  "plane" = "plane",
}

export enum GeometryType {
  "Polygon" = "Polygon",
  "MultiPolygon" = "MultiPolygon",
}

/** We use `| number[]` because when parsing the geoJson data, it just detects it as number[] */
export type geoJSONCoordinate =
  | [longitude: number, latitude: number]
  | number[];

/** We use `| number[][]` because when parsing the geoJson data, it just detects it as number[][] */
export type geoJSONLinearRing = geoJSONCoordinate[] | number[][];

/**
 * The exteriorRing will be the surface, and the interiorRing is an option hole in the surface.
 * See: https://tools.ietf.org/html/rfc7946#section-3.1.6
 */
export type geoJSONGeometryPolygonCoordinates =
  | [exteriorRing: geoJSONLinearRing, interiorRing: geoJSONLinearRing]
  | number[][][];

/** We use `| number[][][][]` because when parsing the geoJson data, it just detects it as number[][][][] */
export type geoJSONGeometryMultiPolygonCoordinates =
  | geoJSONGeometryPolygonCoordinates[]
  | number[][][][];

/** Either Polygon or MultiPolygonCoordinates */
export type geoJSONGeometryCoordinates =
  | geoJSONGeometryPolygonCoordinates
  | geoJSONGeometryMultiPolygonCoordinates;

export type CountriesStatusAtomStructure = { [key: number] : CountryStatus };

/** The status of a country (selected, incorrect, etc) */
export enum CountryStatus { // unSelected,
  selectableUnselected,
  selected,
  correct,
  incorrect,
  disabled
}

// =-=--=-=-=-=-=-=-=-=-==--=
// Game Mode - Game Data
export enum GameMode {
  SelectGameMode = "Select Game Mode",
  SelectSubregion = "Select Subregion",
  SelectCountryInSubregion = "Select Country in Subregion",
  ExploreMap = "Explore Map",
  SelectContinent = "Select Continent",
  SelectCountryInContinent = "Select Country in Continent",
  SelectCountryInWorld = "Select Country World",
  // SelectSubregion = "Select Subregion",
}

