import { atom, atomFamily, selector, selectorFamily } from "recoil";

// import { gmSelectedSubregionState } from "../gameData/atom";

import {
  COUNTRIES_SUBREGIONS,
  // CAMERA_ORBIT_RADIUS,
  INITIAL_CAMERA_SPHERICAL_COORDS,
  // COUNTRIES_DATA,
} from "../../constants";

import {
  CountryStatus,
  // CountryStatusFamilySelectorId,
  diffPosition,
  sphericalCoordinates,
  CountriesStatusAtomStructure,
} from "../../types";

export const textState = atom<string>({
  key: "textState", // unique ID (with respect to other atoms/selectors)
  default: "", // default value (aka initial value)
});

export const cameraDxDyState = atom<diffPosition>({
  key: "cameraDxDyState", // unique ID (with respect to other atoms/selectors)
  default: { dx: 0, dy: 0 }, // default value (aka initial value)
});

export const initialCameraSphericalCoordsState = atom<sphericalCoordinates>({
  key: "initialCameraSphericalCoordsState", // unique ID (with respect to other atoms/selectors)
  default: INITIAL_CAMERA_SPHERICAL_COORDS, // default value (aka initial value)
});

export const currentCameraSphericalCoordsState = atom<sphericalCoordinates>({
  key: "currentCameraSphericalCoordsState", // unique ID (with respect to other atoms/selectors)
  default: INITIAL_CAMERA_SPHERICAL_COORDS, // default value (aka initial value)
});

// export const countryStatusFamilyState = atomFamily({
// export const countryStatusFamilyState = atomFamily<CountryStatus, number>({
//   key: "countryStatusFamilyState",
//   default: {},
// });

// export const countriesStatus = atom<CountriesStatusAtomStructure>({
//   key: "countriesStatus",
//   deafult: {},
// });

/**
 *
 * This stores (by country Id) the country's status.
 * This is useful since its value is used directly within
 * each individual country, for better rendering performance.
 *
 */
// export const countryStatusFamilyState = atomFamily<CountryStatus, number>({
// export const countryStatusFamilyState = atomFamily<
//   CountryStatusFamilySelectorId,
//   number
// >({
//   key: "countryStatusFamilyState",
//   default: CountryStatusFamilySelectorId.selectableUnselected,
// });

/**
 * Listing all country ids that have a specific Country Status
 *
 * The different family selectors is from CountryStatusFamilySelectorId. E.g.:
 *    selected,
 *    correct,
 *    incorrect,
 *    disabled
 *
 */
// export const allCountryIndexesForCountryStatusTypeFamilyState = atomFamily<
//   number[],
//   CountryStatusFamilySelectorId
// >({
//   key: "allCountryIndexesForCountryStatusTypeFamilyState",
//   default: [],
// });

/**
 * An object where the countryIds are keys.
 * And the value is the current CountryStatus (e.g. selected)
 */
export const countriesStatusState = atom<CountriesStatusAtomStructure>({
  key: "countriesStatus",
  default: {},
});

export const countryStatusState = atomFamily<number, CountryStatus>({
  key: "countryStatusState",
  default: undefined,
  // get: (countryId: number) => ({ get }) => {
  //   // return get(myNumberState) * multiplier;
  //   const countriesStatus = get(countriesStatusState);
  //   return countriesStatus?.[countryId];

  // },

  // // optional set
  // set: (multiplier) => ({ set }, newValue) => {
  //   set(myNumberState, newValue / multiplier);
  // },
});

/** Returns the status of a particular region */
export const subregionCountryStatusFamilyState = atomFamily<
  CountryStatus | undefined,
  string
>({
  key: "subregionCountryStatusFamilyState",
  /* Initially the game mode is to select a subregion */
  default: undefined,
});

/** This behaves more like a side effect */
export const clearThenDisableAllSubregionsExceptSelectedState = selector({
  key: "disableAllSubregionsExceptState",
  get: () => undefined,
  set: ({ get, set }) => {
    console.log("broken but moving to redux - taken out to no import cycle.");
    // const exceptedSubregion = get(gmSelectedSubregionState);

    // COUNTRIES_SUBREGIONS.forEach((subregion) => {
    //   if (subregion === exceptedSubregion) {
    //     set(subregionCountryStatusFamilyState(subregion), undefined);
    //   } else {
    //     set(
    //       subregionCountryStatusFamilyState(subregion),
    //       CountryStatus.disabled
    //     );
    //   }
    // });
  },
});

/** This behaves more like a side effect */
export const clearAllSubregionsState = selector({
  key: "clearAllSubregionsState",
  get: () => undefined,
  set: ({ set }) => {
    COUNTRIES_SUBREGIONS.forEach((subregion) => {
      set(subregionCountryStatusFamilyState(subregion), undefined);
    });
  },
});
