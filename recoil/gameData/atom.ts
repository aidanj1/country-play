import { atom, selector } from "recoil";
import { COUNTRY_IDS_FOR_SUBREGIONS } from "../../constants";
import { CountryStatus, GameMode } from "../../types";
import {
  countryStatusState,
  subregionCountryStatusFamilyState,
} from "../countries/atom";

/**
 * An object where the countyId is the key.
 * And the value is the current CountryStatus (e.g. selected)
 */
export const gameModeState = atom<GameMode>({
  key: "gameModeState",
  /* Initially the game mode is to select a subregion */
  default: GameMode.SelectCountryInWorld,
});

/* gm prefix means related to "game mode" */
// export const gmSelectedSubregionState = atom<string | undefined>({
const _gmSelectedSubregionLowLevelState = atom<string | undefined>({
  key: "_gmSelectedSubregionState",
  /* Initially the game mode is to select a subregion */
  default: undefined,
});

export const gmSelectedSubregionState = selector<string | undefined>({
  key: "gmSelectedSubregionState",
  get: ({ get }) => get(_gmSelectedSubregionLowLevelState),
  set: ({ get, set }, newSelectedSubregion) => {
    const currentSelectedSubregion = get(_gmSelectedSubregionLowLevelState);

    // Don't change anything if it's already selected.
    if (currentSelectedSubregion === newSelectedSubregion) return;

    if (typeof currentSelectedSubregion === "string") {
      set(
        subregionCountryStatusFamilyState(currentSelectedSubregion),
        undefined
      );
    }

    if (typeof newSelectedSubregion === "string") {
      set(
        subregionCountryStatusFamilyState(newSelectedSubregion),
        CountryStatus.selected
      );
    }

    set(_gmSelectedSubregionLowLevelState, newSelectedSubregion);
  },
});

/** This acts more as a side effect */
export const gmDeselectAllCountriesState = selector({
  key: "gmDeselectAllCountriesState",
  // get: ({ get }) => undefined,
  get: () => undefined,
  set: ({ get, set }, newSelectedSubregion) => {
    // set(myAtom, newValue instanceof DefaultValue ? newValue : newValue / 100),
    const currentSelectedSubregion = get(_gmSelectedSubregionLowLevelState);

    if (typeof currentSelectedSubregion === "string") {
      const prevSelectedCountryIds =
        COUNTRY_IDS_FOR_SUBREGIONS[currentSelectedSubregion];

      prevSelectedCountryIds?.forEach((countryId) => {
        set(countryStatusState(countryId), undefined);
      });
    }

    if (typeof newSelectedSubregion === "string") {
      const newSelectedCountryIds =
        COUNTRY_IDS_FOR_SUBREGIONS[newSelectedSubregion];

      newSelectedCountryIds?.forEach((countryId) => {
        set(countryStatusState(countryId), CountryStatus.selected);
      });
    }

    set(_gmSelectedSubregionLowLevelState, newSelectedSubregion);
  },
});

/* gm prefix means related to "game mode" */
export const gmSelectedContinentState = atom<string | undefined>({
  key: "gmSelectedContinentState",
  /* Initially the game mode is to select a subregion */
  default: undefined,
});

/* gm prefix means related to "game mode" */
export const gmSelectedCountryState = atom<number | undefined>({
  key: "gmSelectedCountryState",
  /* Initially the game mode is to select a subregion */
  default: undefined,
});

export const gmSelectedItemState = selector({
  key: "gmSelectedItemState",
  get: ({ get }) => {
    const gameMode = get(gameModeState);

    switch (gameMode) {
      case GameMode.ExploreMap:
        return get(gmSelectedCountryState);
        break;
      case GameMode.SelectContinent:
        return get(gmSelectedContinentState);
        break;
      case GameMode.SelectSubregion:
        return get(gmSelectedSubregionState);
        break;
      default:
        return undefined;
        break;
    }

    return undefined;
  },
});
