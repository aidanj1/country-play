# Country Play

<p>
  <!-- iOS -->
  <img alt="Supports Expo iOS" longdesc="Supports Expo iOS" src="https://img.shields.io/badge/iOS-4630EB.svg?style=flat-square&logo=APPLE&labelColor=999999&logoColor=fff" />
  <!-- Android -->
</p>

This sample project is an exploration of [React Native](https://reactnative.dev/), [Expo](https://expo.io/), [react-three-fiber](https://react-three-fiber), [three.js](https://threejs.org/), [geojson data](https://geojson.org/), and [recoil.js](https://recoiljs.org/).

It will render a globe of the earth whereby you can click on countries to see their name and flag.

![Screenshot](assets/screenshot.jpg?raw=true "Screenshot")

## Requirements

Note that this project will not run well on simulator, it is best to run on a physical device (currently only tested on iOS).

## 🚀 How to use

- Have the [Expo Go app](https://apps.apple.com/ca/app/expo-go/id982107779) installed on an iOS device.
- Run `yarn` or `npm install`
- Ensure you have [`expo-cli`](https://docs.expo.dev/) installed.
- Run [`expo start`](https://docs.expo.io/versions/latest/workflow/expo-cli/)
- Ensure your iOS device is on the same network, then through the iOS camera app scan the displayed QR code. This will open the project.
- Have fun!


